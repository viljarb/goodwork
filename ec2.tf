resource "aws_security_group" "useless-box" {
  name        = "tf-useless-box"
  description = "Used in the terraform"
  vpc_id      = module.vpc.vpc_id

  tags = {
    Name = "tf-infosec-test"
  }


  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [
        "0.0.0.0/0",
    ]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
        "0.0.0.0/0",
    ]
  }


  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 53
    to_port     = 53
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 53
    to_port     = 53
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "useless-box" {
  tags = {
    Name = "tf-infosec-test"
    Team = "infosec"
    Test = "Yes"
  }

  instance_type               = "t2.micro"
  ami                         = "ami-077a5b1762a2dde35"
  key_name                    = "infosec"
  vpc_security_group_ids      = [aws_security_group.infosec-test.id]
  subnet_id                   = module.vpc.public_subnets[0]
  associate_public_ip_address = "true"
}

output "useless-box-connect" {
  value = "ssh -i ~/private.pem ubuntu@${aws_instance.useless-box.public_ip}"
}
