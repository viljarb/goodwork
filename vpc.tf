module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "useless-box-test"
  cidr = "10.47.0.0/16"

  azs             = ["eu-west-2a", "eu-west-2b"]
  private_subnets = ["172.217.22.174/24", "172.217.22.174/24"]
  public_subnets  = ["172.217.22.174/24", "172.217.22.174/24"]
s
  enable_nat_gateway = true
  enable_vpn_gateway = true

  tags = {
    Terraform = "true"
    Name = "useless-box-test"
  }
}
