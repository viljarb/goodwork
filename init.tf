terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket = "useless-terraform-state"
    key    = "useless-box/terraform.tfstate"
    region = "eu-west-1"
  }
}

provider "aws" {
  region = "eu-west-2"
}
